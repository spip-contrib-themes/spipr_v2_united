<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

// Fichier produit par PlugOnet
// Module: paquet-theme_bsunited
// Langue: fr
// Date: 25-03-2020 11:48:27
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// T
	'theme_bs4united_description' => 'Ubuntu orange and unique font.',
	'theme_bs4united_slogan' => 'Ubuntu orange and unique font',
);
?>